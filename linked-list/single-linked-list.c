#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
  int data;
  Node *next;
} Node;

Node *head = NULL, *tail = NULL;

Node *new_node(int data)
{
  Node *node = (Node *)malloc(sizeof(Node));
  node->data = data;
  node->next = NULL;
}

void print_list()
{
  Node *node_pointer = head;

  printf("Linked List: ");
  while (node_pointer != NULL)
  {
    if (node_pointer->next == NULL)
    {
      printf("[%p](%d | %p)\n", node_pointer, node_pointer->data, node_pointer->next);
    }
    else
    {
      printf("[%p](%d | %p) -> ", node_pointer, node_pointer->data, node_pointer->next);
    }
    node_pointer = node_pointer->next;
  }
}

void insert_head(int data)
{
  Node *node = new_node(data);

  if (head == NULL)
  {
    head = node;
    tail = node;

    return;
  }

  node->next = head;
  head = node;
}

void insert_tail(int data)
{
  Node *node = new_node(data);

  if (head == NULL)
  {
    head = node;
    tail = node;

    return;
  }

  tail->next = node;
  tail = node;
}

void delete_head()
{
  Node *temp = head;
  head = head->next;
  free(temp);
}

void delete_tail()
{
  Node *node_pointer = head;

  while (node_pointer->next != tail) 
  {
    node_pointer = node_pointer->next;
  }

  free(tail);
  node_pointer->next = NULL;
  tail = node_pointer;
}

int main()
{
  insert_tail(10);
  print_list();

  insert_tail(20);
  print_list();

  insert_tail(-10);
  print_list();

  delete_head();
  print_list();

  insert_tail(-20);
  print_list();

  insert_head(30);
  print_list();

  insert_head(40);
  print_list();

  delete_head();
  print_list();

  delete_tail();
  print_list();

  return 0;
}
