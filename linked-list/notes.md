## Linked List

Sebuah bentuk penyimpanan data secara linear dimana tidak didefinisikan jumlah ruangan yang diperlukan diawal, namun jumlah ruangan akan bertambah on-demand.

Linked List direpresentasikan sebagai untaian dari ruangan (Node). 


Beberapa bentuk linked list yang populer:
1. Single Linked List
2. Double Linked List
3. Circular Single Linked List
4. Circular Double Linked List

Terdapat dua istilah yang seringkali dipakai di dalam Linked List:
1. Head -> kepala atau awal dari suatu linked list.
2. Tail -> akhir dari suatu linked list.

Operasi umum yang terjadi di dalam Linked List:
1. Insert Node (Insert Before Node, Insert After Node, Insert Tail)
2. Delete Node (Delete Before Node, Delete After Node, Delete on Position, Delete Head, Delete Tail)
3. Print Out

### Single Linked List

Berikut ini adalah contoh representasi single linked list node:
[data | address_next_node]

Dalam bahasa C, Node didefinisikan dengan menggunakan ```struct```

```c

struct Node {
  <data-type> data;
  Node *next;
}

```

Berikut ini adalah contoh representasi single linked list:
[data | address_next_node] -> [data | address_next_node] -> [data | address_next_node] -> [data | address_next_node]

#### Operations

##### Print

Print untuk single linked list dapat dilakukan dengan menggunakan while. Selama pointer yang digunakan untuk menunjukkan posisi node yang diakses masih berbeda dengan tail. Maka print terus.

##### Insert

1. Insert Tail
   Linked list: [1] -> [2] -> [3 | X]

   Head: [1]
   Tail: [3 | X]

   Case: Insert Tail data = 18 (new_node = [18])

   Process:
   1. tail->next = new_node (Connect tail to new_node)
      Linked list: [1] -> [2] -> [3] -> [18]
      Head: [1]
      Tail: [3]

   2. tail = new_node (Set new_node as the new tail)
      Linked list: [1] -> [2] -> [3] -> [18]
      Head: [1]
      Tail: [18]

2. Insert Head
   Linked list: [1] -> [2] -> [3 | X]

   Head: [1]
   Tail: [3 | X]

   Case: Insert Head data = 18 (new_node = [18])

   Process:
   1. new_node->next = head;
      Linked list: [18] -> [1] -> [2] -> [3]
      Head: [1]
      Tail: [3]
      
   2. head = new_node;
      Linked list: [18] -> [1] -> [2] -> [3]
      Head: [18]
      Tail: [3]

##### Delete

1. Delete Tail
   Linked list: [1] -> [2] -> [3 | X]

   Head: [1]
   Tail: [3 | X]

   Process:
   1. Find the last node before tail node. (Use looping)
   2. free(tail)
   3. node_pointer->next = NULL
   4. tail = node_pointer

2. Delete Head
   Linked list: [1] -> [2] -> [3 | X]

   Head: [1]
   Tail: [3 | X]

   Process:
   1. Node *temp = head (Store the head address to a temporary variable)
   2. head = head->next (Store the head's next node address to head variable)
   3. free(temp) (Free the old head memory, return it back to computer)

