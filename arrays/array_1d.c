#include <stdio.h>

int main()
{
  int N;
  printf("Input N: ");
  scanf("%d", &N);

  int numbers[N];

  printf("Input list of numbers: ");
  int i;
  for (int i = 0; i < N; i++)
  {
    scanf("%d", &numbers[i]);
  }

  int find;
  printf("Number to be find it's position: ");
  scanf("%d", &find);

  for (i = 0; i < N; i++)
  {
    if (numbers[i] == find)
    {
      printf("Number %d found in %d position.\n", find, i + 1);
      return 0;
    }
  }

  printf("Number %d is not found.\n", find);

  return 0;
}
