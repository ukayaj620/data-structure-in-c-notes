#include <stdio.h>

int main ()
{
  int array[5] = {1, 2, 3, 4, 5};

  printf("array[0] address: %p\n", array);
  printf("array[1] address: %p\n", (array + 1));
  printf("array[2] address: %p\n", (array + 2));
  printf("array[3] address: %p\n", (array + 3));

  printf("\narray[0] value: %d\n", *array);
  printf("array[1] value: %d\n", *(array + 1));
  printf("array[2] value: %d\n", *(array + 2));
  printf("array[3] value: %d\n", *(array + 3));

  return 0;
}
