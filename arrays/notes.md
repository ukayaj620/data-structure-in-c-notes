## Array

Array -> melakukan penyimpanan data, berbentuk berupa list maupun matrix.

Array bisa berapa dimensi? n-dimension.

- Array 1D:
  list [][][][][]
- Array 2D:
  matrix
  [][][][]
  [][][][]
  [][][][]
- Array 3D:
  ruang 3D
    [][][][]
  [][][][][]
  [][][][][]
  [][][][]


### 1D Array

1D array memiliki bentuk seperti sebuah list yang terdiri dari N ruangan.

Representasi Bentuk dari 1D Array: [_, _, _, _, _]
                                    0  1  2  3  4

### 2D Array

2D array memiliki bentuk seperti matrix 2 dimensi dengan width dan height, seperti yang dipelajari pada matematika aljabar linear. Bisa dikatakan adalah array dari array 1D.

arr_2d = [
  [1, 2, 3, 4, 5],
  [6, 7, 8, 9, 10],
  [_, _, _, _, _],
  [_, _, _, _, _],
  [_, _, _, _, _],
]

arr_2d[0] = [1, 2, 3, 4, 5]
arr_2d[1] = [6, 7, 8, 9, 10]
arr_2d[0][3] = 4


### 3D Array

3D array memiliki bentuk seperti matrix 3 dimensi. Jika dipikirkan dalam bentuk bangun ruang, bisa dikatakan berbentuk balok, prisma segiempat, maupun kubus.

arr_3d = [
  [
    [1, 2, 3, 4], [5, 6, 7, 8],
  ],
  [
    [9, 10, 11, 12], [13, 14, 15, 16]
  ],
]

arr_3d[1] = [[9, 10, 11, 12], [13, 14, 15, 16]]
arr_3d[0][0] = [1, 2, 3, 4]
arr_3d[0][1][3] = 8
