#include <stdio.h>
#define N 100

/*
  Program untuk operasi matrix.
*/

void print_matrix(int matrix[N][N], int row, int col, const char* label)
{
  int i, j;

  printf("\n%s", label);

  printf("\n[\n");
  for (i = 0; i < row; i++)
  {
    printf("    [ ");
    for (j = 0; j < col; j++)
    {
      printf("%d ", matrix[i][j]);
    }
    printf("]\n");
  }
  printf("]\n");
}

int main ()
{
  int matrix_1[N][N], matrix_2[N][N];

  int matrix_1_row, matrix_1_col, matrix_2_row, matrix_2_col;

  printf("Please input Matrix 1 size\n");
  printf("N row: "); scanf("%d", &matrix_1_row);
  printf("N col: "); scanf("%d", &matrix_1_col);

  printf("\nPlease input Matrix 2 size\n");
  printf("N row: "); scanf("%d", &matrix_2_row);
  printf("N col: "); scanf("%d", &matrix_2_col);

  if (matrix_1_row != matrix_2_row || matrix_1_col != matrix_2_col)
  {
    printf("Both matrix size is not the same.");
    return 0;
  }

  int row = matrix_1_row;
  int col = matrix_1_col;

  int i, j;

  printf("\nPlease input Matrix 1 value\n");
  for (i = 0; i < row; i++)
  {
    for (j = 0; j < col; j++)
    {
      printf("Insert position (%d, %d): ", i, j);
      scanf("%d", &matrix_1[i][j]);
    }
  }

  printf("\nPlease input Matrix 2 value\n");
  for (i = 0; i < row; i++)
  {
    for (j = 0; j < col; j++)
    {
      printf("Insert position (%d, %d): ", i, j);
      scanf("%d", &matrix_2[i][j]);
    }
  }

  print_matrix(matrix_1, row, col, "Matrix 1");
  print_matrix(matrix_2, row, col, "Matrix 2");

  int matrix_sum_result[N][N] = {0};

  for (i = 0; i < row; i++)
  {
    for (j = 0; j < col; j++)
    {
      matrix_sum_result[i][j] = matrix_1[i][j] + matrix_2[i][j];
    }
  }

  print_matrix(matrix_sum_result, row, col, "Matrix Sum Result");

  return 0;
}
