#include <stdio.h>

int main()
{
  int number = 10;

  int *pointer_number = &number;

  printf("Number VALUE: %d\n", number);
  printf("Number ADDRESS: %p\n", &number);

  printf("\nPointer Number VALUE: %p\n", pointer_number);
  printf("Pointer Number ADDRESS: %p\n", &pointer_number);

  int **pointer_pointer_number = &pointer_number;

  printf("\nPointer Pointer Number VALUE: %p\n", pointer_pointer_number);
  printf("Pointer Pointer Number ADDRESS: %p\n", &pointer_pointer_number);

  return 0;
}
