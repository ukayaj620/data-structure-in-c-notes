## Pointer

Apa itu pointer? 
Sebuah variable yang dapat menyimpan address dari suatu memori.

Setiap variable, baik variable biasa maupun variable pointer pasti memiliki address, karena disanalah alamat dimana data dalam variable tersebut tersimpan pada memori.

angka = 100

angka (value -> 100)
angka (address -> 0xFFA567)

*angka = 0xFFA567

Syntax:
- * digunakan untuk mengakses value yang tersimpan dalam suatu address memori
- & digunakan untuk mendapatkan address dari suatu variable yang telah dideklarasikan.
